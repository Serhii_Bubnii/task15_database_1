1.1--------------------------------
SELECT DISTINCT
    maker, type
FROM
    labor_sql.product
ORDER BY maker

1.2--------------------------------
SELECT 
    model, ram, screen, price
FROM
    labor_sql.laptop
WHERE
    price > 1000
	
1.3--------------------------------
SELECT 
    code, model, color, type, price
FROM
    labor_sql.printer
WHERE
    color = 'y'
ORDER BY price DESC

1.4--------------------------------
SELECT 
    model, speed, hd, cd, price
FROM
    labor_sql.pc
WHERE
    cd = '12x' OR cd = '24x' AND price < 600
ORDER BY speed DESC

1.5--------------------------------
SELECT 
    name, class
FROM
    labor_sql.ships
ORDER BY name

1.6--------------------------------
SELECT 
    model, speed, hd, cd, price
FROM
    labor_sql.pc
WHERE
    speed >= 500 and price < 800
ORDER BY price DESC

1.7--------------------------------
SELECT 
    *
FROM
    labor_sql.printer
WHERE
    NOT type = 'Matrix' AND price < 300
ORDER BY type DESC

1.8--------------------------------
SELECT 
   model, speed
FROM
    labor_sql.pc
WHERE
    price >= 400 AND price <= 600
ORDER BY hd

1.9--------------------------------
SELECT 
    pc.model, pc.speed, pc.hd
FROM
    labor_sql.pc,
    labor_sql.product
WHERE
    hd = 10
        OR hd = 20 AND price <= 600
        AND product.maker = 'A'
GROUP BY code

1.10--------------------------------
SELECT 
    model, speed, hd, price
FROM
    labor_sql.laptop
WHERE
    screen >= 12
ORDER BY price DESC

1.11--------------------------------
SELECT 
    model, type, price
FROM
    labor_sql.printer
WHERE
    price < 300
ORDER BY type DESC

1.12--------------------------------
SELECT 
    model, ram, price
FROM
    labor_sql.laptop
WHERE
    ram = 64
ORDER BY screen

1.13--------------------------------
SELECT 
    model, ram, price
FROM
    labor_sql.pc
WHERE
    ram > 64
ORDER BY hd


2.1--------------------------------
SELECT 
    *
FROM
    labor_sql.pc
WHERE
    model RLIKE '1{2,}'
ORDER BY hd

2.2--------------------------------
SELECT 
    *
FROM
    labor_sql.outcome
WHERE
    MONTH(Date) = 3
ORDER BY code

2.3--------------------------------
SELECT 
    *
FROM
    labor_sql.outcome_o
WHERE
    DAY(date) = 14

2.4--------------------------------
SELECT 
    *
FROM
    labor_sql.ships
WHERE
    name rlike '^W.*n$'

2.5--------------------------------
SELECT 
    *
FROM
    labor_sql.ships
WHERE
    name RLIKE 'e{2}'


2.6--------------------------------
SELECT 
    name, launched
FROM
    labor_sql.ships
WHERE
    name RLIKE '[^a]$'

2.7--------------------------------
SELECT 
    name
FROM
    labor_sql.battles
WHERE
    name RLIKE '[[:<:]].+[[:<:]].*[^c][[:>:]]'

2.8--------------------------------
SELECT 
    *
FROM
    labor_sql.trip
WHERE
    HOUR(time_out) >= 12 and HOUR(time_out) < 17

2.9--------------------------------
SELECT 
    *
FROM
    labor_sql.trip
WHERE
    HOUR(time_in) >= 17 and HOUR(time_in) < 23

2.10--------------------------------
SELECT 
    date
FROM
    labor_sql.pass_in_trip
WHERE
   place RLIKE '[[:<:]]1'
   
3.1--------------------------------
SELECT 
    product.maker, product.type, pc.speed, pc.hd
FROM
    labor_sql.pc,
    labor_sql.product
WHERE
	pc.model = product.model and
    pc.hd <= 8

3.2--------------------------------
SELECT 
    product.maker
FROM
    labor_sql.pc,
    labor_sql.product
WHERE
    pc.model = product.model
        AND pc.speed >= 600
GROUP BY product.maker

3.3--------------------------------
SELECT 
    product.maker
FROM
    labor_sql.laptop,
    labor_sql.product
WHERE
    laptop.model = product.model
        AND laptop.speed <= 500
GROUP BY product.maker

3.4--------------------------------
3.5--------------------------------
3.6--------------------------------
3.7--------------------------------
3.8--------------------------------