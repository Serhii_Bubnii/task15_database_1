SELECT 
    model, type, price
FROM
    labor_sql.printer
WHERE
    price < 300
ORDER BY type DESC