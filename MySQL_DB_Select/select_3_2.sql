SELECT 
    product.maker
FROM
    labor_sql.pc,
    labor_sql.product
WHERE
    pc.model = product.model
        AND pc.speed >= 600
GROUP BY product.maker