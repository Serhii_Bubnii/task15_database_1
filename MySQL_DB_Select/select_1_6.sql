SELECT 
    model, speed, hd, cd, price
FROM
    labor_sql.pc
WHERE
    speed >= 500 and price < 800
ORDER BY price DESC