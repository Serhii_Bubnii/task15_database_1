SELECT 
    *
FROM
    labor_sql.printer
WHERE
    NOT type = 'Matrix' AND price < 300
ORDER BY type DESC