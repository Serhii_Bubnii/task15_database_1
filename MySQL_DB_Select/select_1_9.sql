SELECT 
    pc.model, pc.speed, pc.hd
FROM
    labor_sql.pc,
    labor_sql.product
WHERE
    hd = 10
        OR hd = 20 AND price <= 600
        AND product.maker = 'A'
GROUP BY code