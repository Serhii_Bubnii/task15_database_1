SELECT 
    model, speed, hd, price
FROM
    labor_sql.laptop
WHERE
    screen >= 12
ORDER BY price DESC