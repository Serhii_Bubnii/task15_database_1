SELECT 
    product.maker, product.type, pc.speed, pc.hd
FROM
    labor_sql.pc,
    labor_sql.product
WHERE
	pc.model = product.model and
    pc.hd <= 8
