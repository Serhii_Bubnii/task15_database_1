SELECT 
    product.maker
FROM
    labor_sql.laptop,
    labor_sql.product
WHERE
    laptop.model = product.model
        AND laptop.speed <= 500
GROUP BY product.maker