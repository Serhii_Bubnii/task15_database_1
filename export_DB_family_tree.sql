CREATE DATABASE  IF NOT EXISTS `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mydb`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `family_satellites`
--

DROP TABLE IF EXISTS `family_satellites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `family_satellites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(45) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `birth_date` date NOT NULL,
  `death_date` date DEFAULT NULL,
  `birth_place` varchar(45) NOT NULL,
  `death_place` varchar(45) DEFAULT NULL,
  `marriage_date` date DEFAULT NULL,
  `family_tree_id` int(11) NOT NULL,
  `sexs_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`family_tree_id`,`sexs_id`),
  KEY `fk_family_satellites_family_tree1_idx` (`family_tree_id`),
  KEY `fk_family_satellites_sexs1_idx` (`sexs_id`),
  CONSTRAINT `fk_family_satellites_family_tree1` FOREIGN KEY (`family_tree_id`) REFERENCES `family_tree` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_satellites_sexs1` FOREIGN KEY (`sexs_id`) REFERENCES `sexs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family_satellites`
--

LOCK TABLES `family_satellites` WRITE;
/*!40000 ALTER TABLE `family_satellites` DISABLE KEYS */;
/*!40000 ALTER TABLE `family_satellites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `family_tree`
--

DROP TABLE IF EXISTS `family_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `family_tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(45) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `birth_date` date NOT NULL,
  `death_date` date DEFAULT NULL,
  `birth_place` varchar(45) NOT NULL,
  `death_place` varchar(45) DEFAULT NULL,
  `credit_card_number` bigint(12) DEFAULT NULL,
  `family_tree_id` int(11) NOT NULL,
  `sexs_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`family_tree_id`,`sexs_id`),
  KEY `fk_family_tree_family_tree1_idx` (`family_tree_id`),
  KEY `fk_family_tree_sexs1_idx` (`sexs_id`),
  CONSTRAINT `fk_family_tree_family_tree1` FOREIGN KEY (`family_tree_id`) REFERENCES `family_tree` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_tree_sexs1` FOREIGN KEY (`sexs_id`) REFERENCES `sexs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family_tree`
--

LOCK TABLES `family_tree` WRITE;
/*!40000 ALTER TABLE `family_tree` DISABLE KEYS */;
/*!40000 ALTER TABLE `family_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `family_tree_has_family_values`
--

DROP TABLE IF EXISTS `family_tree_has_family_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `family_tree_has_family_values` (
  `family_tree_id` int(11) NOT NULL,
  `family_values_id` int(11) NOT NULL,
  PRIMARY KEY (`family_tree_id`,`family_values_id`),
  KEY `fk_family_tree_has_family_values_family_values1_idx` (`family_values_id`),
  KEY `fk_family_tree_has_family_values_family_tree_idx` (`family_tree_id`),
  CONSTRAINT `fk_family_tree_has_family_values_family_tree` FOREIGN KEY (`family_tree_id`) REFERENCES `family_tree` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_tree_has_family_values_family_values1` FOREIGN KEY (`family_values_id`) REFERENCES `family_values` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family_tree_has_family_values`
--

LOCK TABLES `family_tree_has_family_values` WRITE;
/*!40000 ALTER TABLE `family_tree_has_family_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `family_tree_has_family_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `family_values`
--

DROP TABLE IF EXISTS `family_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `family_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_of_value` varchar(45) NOT NULL,
  `estimated_cost` decimal(20,2) NOT NULL,
  `maximum_cost` decimal(20,2) NOT NULL,
  `minimum_cost` decimal(20,2) NOT NULL,
  `code_in_value_catalog` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family_values`
--

LOCK TABLES `family_values` WRITE;
/*!40000 ALTER TABLE `family_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `family_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sexs`
--

DROP TABLE IF EXISTS `sexs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sexs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sex` enum('Male','Female') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sexs`
--

LOCK TABLES `sexs` WRITE;
/*!40000 ALTER TABLE `sexs` DISABLE KEYS */;
/*!40000 ALTER TABLE `sexs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-28 18:24:36
